# GPSOrganisationTracker Wiki

## Project Title
**GPSOrganisationTracker**: Accurately Tracking Buses in Remote Areas

## Team

| Name                      | Role                         | Email                             |
|---------------------------|------------------------------|-----------------------------------|
| Rădulescu Vlad           | Chief Executive Officer (CEO) | vladr2001.vr@gmail.com            |
| Roșca Cvintilian         | Chief Technical Officer (CTO) | quintilianrosca2000@gmail.com     |
| Dragomir Andrei-Valentin | Chief Operating Officer (COO) | andrei31.dg@gmail.com             |
| Dalimon Iarina           | Chief Marketing Officer (CMO) | iarinadalimon@gmail.com           |
| Stoian Dragoș-Andrei     | Software Developer (SD)       | dragos.stoian36@yahoo.com         |
| Robert Coman             | Software Developer (SD)       | robert.coman1205@stud.acs.pub.ro  |

## Problem Statement

Residents in remote areas often face uncertainty regarding bus arrival times. The lack of accurate information can lead to long waiting periods and inconvenience, especially in areas with harsh weather conditions. There is also no way to know if the bus is running late or having problems.

## Solution / Value Proposition

**GPSOrganisationTracker** is a mobile and web-based platform that offers real-time tracking of buses in remote locations. The platform provides:
- Route Visualization
- Live Location Tracking of Buses
- Alerts for Delays or Route Changes

## Customer Segments

1. People that use public transportation in remote areas (ex: in Bran)
2. Taxi Companies

## Competition

1. Geo Tracker
2. Citymapper
3. Moovit
4. Transit
5. InfoTB

## Advantage over the Competition

- **User-friendly Interface**: Simplified and intuitive design for all age groups.
- **High Precision**: Uses advanced GPS algorithms for accurate tracking.
- **Affordable Pricing**: Cost-effective plans for businesses of all sizes and also for users.
- **24/7 Customer Support**: Round the clock support for all your queries.

## Key Metrics

1. **Active User Growth Rate**: Monitoring monthly sign-ups and active users.
2. **Average Session Time**: Analyzing the time users spend on our platform.
3. **Positive Reviews**: Viewing the number of positive reviews on platforms such as Google Play

## Cost Structure

1. **Platform Development & Maintenance**:
   - **App Development**: Salaries for the programmers responsible with developing and maintaining the application
   - **Server Costs**: Hosting, data storage, and data transfer costs.
   - **Software Licenses**: Licenses for development tools, mapping services, and other third-party services.
   
2. **Marketing & Outreach**:
   - **Advertisements**: Online ads (social media, search engines), local newspapers, and possibly radio in remote areas.
   - **Promotions**: Discounts or free trials to attract initial users and bus companies.
   - **Public Relations**: Engaging with local communities, holding events or seminars about the benefits of the service.
   
3. **Customer Support**:
   - **Salaries**: For a dedicated support team.

4. **Partnership Initiatives**:
   - **Collaboration Expenses**: Working closely with bus operators, which might involve integration costs or partnership fees.

5. **Legal & Compliance**:
   - **Regulatory Approvals**: Depending on the region, you might need permissions to track and display transit data. Also includes the cost for the hiring and the salary for a legal expert.
   - **Insurance**: To cover potential liabilities related to inaccurate data or other unforeseen issues.

## Revenue Streams

1. **Subscription Model**:
   - **Bus Companies**: Offering them premium features like detailed analytics, custom branding, or priority support.
   - **Users**: A premium version for users that offers added features like no ads, offline maps, or personalized notifications.

2. **Ad-Based Revenue**:
   - Displaying local ads, focusing on businesses in remote areas which users might find useful (like cafes, local stores, accommodations).

3. **Licensing**:
   - Licensing the software to municipalities or transit authorities who might want to incorporate it into their existing systems.

## M1 - 01.11.2023

Build the FIKI together with my team. Integrated all the good (probably) ideas of the team into the FIKI.